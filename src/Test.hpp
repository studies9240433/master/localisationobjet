//
// Created by argo on 24/12/18.
//

#ifndef PROJECT_TEST_HPP
#define PROJECT_TEST_HPP


#include <Core/DiscretiseTransform.hpp>


class Test {
public:

    Test();

private:

    void allTest();

    int testDiscretiseTransform();
    void testTuples();
    void affineTransform(int x, int y, Tuples t, int *newX, int *newY);


    void testDistanceHaussdorff();

};


#endif //PROJECT_TEST_HPP
