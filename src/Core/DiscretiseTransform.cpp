//
// Created by argo on 24/12/18.
//

#include <Log/Log.hpp>
#include "DiscretiseTransform.hpp"
#include "AffineTransform.hpp"

DiscretiseTransform::DiscretiseTransform(Mat _img, Mat mod, std::vector<Point2> _contours, double dmin, double dmax, double skewmax, double smax, bool trans_,int _methodSearch, DistanceHausdorff *distanceHausdorff_,
        double Am_, double AM_, double Bm_, double BM_, double Cm_, double CM_, double Dm_, double DM_) {
    xMax = _img.cols;
    yMax = _img.rows;
    xMod = mod.cols;
    yMod = mod.rows;
    methodSearch = _methodSearch;
    img = _img;

    contours = _contours;
    d_min = dmin;
    d_max = dmax;
    skew_max = skewmax;
    s_max = smax;
    trans= trans_;

    Am = Am_;
    AM = AM_;
    Bm = Bm_;
    BM = BM_;
    Cm = Cm_;
    CM = CM_;
    Dm = Dm_;
    DM = DM_;

    float stepX = 1.0 / xMax;
    float stepY = 1.0 / yMax;

    eps = std::min(stepX,stepY)*2;
    Log(logInfo) << eps;

    e=Tuples(stepX,stepY,stepX,stepY,1,1);
    t=Tuples(1.0,0.0,0.0,1.0,0.0,0.0);
    init=Tuples(-1.0,-1.0,-1.0,-1.0,0.0,0.0);
    distanceHausdorff = distanceHausdorff_;
}

bool DiscretiseTransform::test_skew(Tuples t) {
    double v1 = std::sqrt(t.a*t.a+t.c*t.c);
    double v2 = std::sqrt(t.b*t.b+t.d*t.d);
    return std::max(v1/v2,v2/v1) > skew_max;
}

bool DiscretiseTransform::test_s(Tuples t){
    double v1 = std::sqrt(t.a*t.a+t.c*t.c);
    double v2 = std::sqrt(t.b*t.b+t.d*t.d);
    return std::abs(t.a*t.b+t.c*t.d)/(v1*v2) > s_max;
}

bool DiscretiseTransform::test_area(Tuples t) {
    double a = t.a * t.d - t.b * t.c;
    return a >= d_min && a <= d_max;
}

bool DiscretiseTransform::all_test(Tuples t) {
    double a = t.a * t.d - t.b * t.c;
    if(a < d_min || a > d_max) return false;
    double v1 = std::sqrt(t.a*t.a+t.c*t.c);
    double v2 = std::sqrt(t.b*t.b+t.d*t.d);
    if(std::max(v1/v2,v2/v1) > skew_max) return false;
    return std::abs(t.a*t.b+t.c*t.d)/(v1*v2) <= s_max;
}

std::vector<TP> DiscretiseTransform::subdivide(TP tp) {
    std::vector<TP> res;
    std::vector<double> scores;
    double ym,yM,xm,xM,am,aM,bm,bM,cm,cM,dm,dM;
    double yMid = (tp.first.y + tp.second.y) / 2;
    double xMid = (tp.first.x + tp.second.x) / 2;
    double aMid = (tp.first.a + tp.second.a) / 2;
    double bMid = (tp.first.b + tp.second.b) / 2;
    double cMid = (tp.first.c + tp.second.c) / 2;
    double dMid = (tp.first.d + tp.second.d) / 2;

    for(int i=0;i<64;i++){
        if(i & 1){ ym = tp.first.y; yM = yMid; }
        else{ ym = yMid; yM = tp.second.y;}

        if(i & 2){xm = tp.first.x; xM = xMid;}
        else{ xm = xMid; xM = tp.second.x; }

        if(i & 4){ dm = tp.first.d; dM = dMid;}
        else{dm = dMid; dM = tp.second.d;}

        if(i & 8){cm = tp.first.c; cM = cMid;}
        else{cm = cMid; cM = tp.second.c;}

        if(i & 16){bm = tp.first.b; bM = bMid;}
        else{bm = bMid; bM = tp.second.b;}

        if(i & 32){am = tp.first.a; aM = aMid;}
        else{am = aMid; aM = tp.second.a;}

        Tuples t1(am,bm,cm,dm,xm,ym);
        Tuples t2(aM,bM,cM,dM,xM,yM);

        TP tp(t1,t2);
        double score = interesting_cell(tp, listClimbing.size());
        if(score!=-1){
            unsigned int i=0;
            while(i<scores.size() && scores[i]>score) i++;

            res.insert(res.begin()+i,tp);
            scores.insert(scores.begin()+i,score);
           // res.push_back(tp); // TODO try to insert tp by his score ...
        }


    }
    return res;
}

bool DiscretiseTransform::check_cell(TP tp) {
    for(auto d=tp.first.d; d<=tp.second.d; d+=e.d){
        for(auto c=tp.first.c; c<=tp.second.c; c+=e.c){
            for(auto b=tp.first.b; b<=tp.second.b; b+=e.b){
                for(auto a=tp.first.a; a<=tp.second.a; a+=e.a){
                    if(all_test(Tuples(a,b,c,d,0,0)))
                        return true;
                }
            }
        }
    }
    return false;
}

double DiscretiseTransform::interesting_cell(TP tp, int level) {
    bool boule;
    auto vec = transformContours(contours, img, tp.first, boule);
    if(boule) return -1;
    //penser à set le ft
    //penser à set le ff
    double res;

    switch(methodSearch){
    case 0: case 1: case 3: case 4:
            //ft et tt
            res = distanceHausdorff->forward_tf_prim(vec,tf,level);
            if (res > ff) {
                return res;
            }
            break;
            case 2: case 5:
            res = distanceHausdorff->forward_ff_prim(vec,ff,level);
            if(res < tf){
                return 1/(res+1);
            }
            break;
    }
    return -1;
}

void DiscretiseTransform::begin() {
    if(trans){
        beginTrans();
        return;
    }

    Log(logInfo) << Am << " " << AM << " " << Bm << " " << BM << " " << Cm << " " << CM << " " << Dm << " " << DM;
    distanceHausdorff->cleanDeltaPrime();
    distanceHausdorff->computeDeltaPrime(AM-Am+BM-Bm+xMax/2,CM-Cm+DM-Dm+yMax/2,0);
    TP tp(Tuples(Am,Bm,Cm,Dm,0,0),Tuples(AM,BM,CM,DM,xMax,yMax));
    std::vector<TP> tmpTP = subdivide(tp);
    listClimbing.push_back(tmpTP);
}

bool DiscretiseTransform::hasNext() {
    if(trans) return hasNextTrans();
    while (1) {
        if(listClimbing.size()!=0){
            Log(logInfo) << listClimbing.size() << " " << listClimbing[listClimbing.size()-1].size();
            //if(listClimbing[listClimbing.size()-1].size()!=0){
            //    listClimbing[listClimbing.size()-1][0].first.display();
            //}
        }
        //il reste plus rien dans la cellule
        if (listClimbing.empty()) return false;

        std::vector<TP> vecTmp = listClimbing[listClimbing.size() - 1];
        if (vecTmp.empty()) {
            //on remonte tant
            do {
                listClimbing.pop_back();
                //distanceHausdorff->popBackDeltaPrime();
                if (listClimbing.empty()) return false;
            } while (listClimbing[listClimbing.size() - 1].empty());
        } else {
            TP pair = vecTmp[0];
            //vérifier si cette pair contient seulement une transfo, si c'est le cas on la return
            Tuples tL = pair.first;
            Tuples tH = pair.second;
            double w = tH.a - tL.a + tH.b - tL.b + tH.x - tL.x;
            double h = tH.c - tL.c + tH.d - tL.d + tH.y - tL.y;

            std::vector<TP> tmp = listClimbing[listClimbing.size() - 1];
            tmp.erase(tmp.begin(), tmp.begin() + 1);
            listClimbing[listClimbing.size() - 1] = tmp;
            /*Log(logWarning) << "Test";
            tL.display();
            tH.display();*/
            if (h < 1 && w < 1) { // TODO Vérifier si ça marche ...
                if (all_test(tL)) {
                    t = tL;
                    return true;
                }
            } else {
                if (interesting_cell(pair,listClimbing.size()-1)!=-1){ // Peut-on s'en dispenser ... ? Pit-être
                    if (check_cell(pair)) {
                        distanceHausdorff->computeDeltaPrime(w/2, h/2,listClimbing.size()); // TODO See if w and h correct
                        std::vector<TP> tmpTP = subdivide(pair);
                        listClimbing.push_back(tmpTP);
                    }

                }
            }
        }
    }

}

Tuples DiscretiseTransform::getItr() {
    return t;
}


void DiscretiseTransform::beginTrans() {
    t=Tuples(1.0,0.0,0.0,1.0,0.0,0.0);
}

bool DiscretiseTransform::hasNextTrans() {
    if(t.y < yMax - yMod){
        t.y = t.y + 1;
        return true;
    }else{
        t.y=0;
        if(t.x < xMax - xMod){
            t.x = t.x + 1;
            return true;
        }else{
            t.x = 0;
            return false;
        }
    }
}

void DiscretiseTransform::displayT() {
    Log(logInfo) << "t:" << t.a << " " << t.b << " " << t.c << " " << t.d << " " << t.x << " " << t.y;
}

void DiscretiseTransform::setTf(double v) {
    tf = v;
}

void DiscretiseTransform::setFf(double v) {
    ff = v;
}


/*
bool DiscretiseTransform::test1() {

    if(t.y < yMax / yMod){
        t.y = t.y + e.y;
        return true;
    }else {
        t.y = 0;
        if (t.x < xMax / xMod) {
            t.x = t.x + e.x;
            return true;
        } else {
            t.x = 0;
            if(t.d < float(yMax) / yMod){
                t.d = t.d + e.d;
                return true;
            }else{
                t.d = 1;
                if(t.c < float(yMax) / yMod){
                    t.c = t.c + e.c;
                    return true;
                }else{
                    t.c = 0;
                    if(t.b < float(xMax) / xMod){
                        t.b = t.b + e.b;
                        return true;
                    }else{
                        t.b = 0;
                        if(t.a < float(xMax) / xMod){
                            t.a = t.a + e.a;
                            return true;
                        }else {
                            t.a = 1;
                            return false;
                        }
                    }
                }
            }
        }
    }
}

bool DiscretiseTransform::test2() {
    if(all_trans_done){
        all_trans_done = false;
        do{
            if(t.d <= 1){
                t.d = t.d + e.d;
            }else{
                t.d = -1;
                if(t.c <= 1){
                    t.c = t.c + e.c;
                }else{
                    t.c = -1;
                    if(t.b <= 1){
                        t.b = t.b + e.b;
                    }else{
                        t.b = -1;
                        if(t.a <= 1){
                            t.a = t.a + e.a;
                        }else {
                            t.a = -1;
                            return false;
                        }
                    }
                }
            }
        }while(!all_test(t));
    }else{
        if(t.y < yMax){
            t.y = t.y + e.y;
        }else {
            t.y = 0;
            if (t.x < xMax) {
                t.x = t.x + e.x;
            } else {
                t.x = 0;
                all_trans_done = true;
            }
        }
    }
    return true;
<<<<<<< HEAD
}

void DiscretiseTransform::beginTrans() {
    t=Tuples(1.0,0.0,0.0,1.0,0.0,0.0);
}

bool DiscretiseTransform::hasNextTrans() {
    if(t.y < yMax - yMod){
        t.y = t.y + 1;
        return true;
    }else{
        t.y=0;
        if(t.x < xMax - xMod){
            t.x = t.x + 1;
            return true;
        }else{
            t.x = 0;
            return false;
        }
    }
}

void DiscretiseTransform::displayT() {
    Log(logInfo) << "t:" << t.a << " " << t.b << " " << t.c << " " << t.d << " " << t.x << " " << t.y;
}
=======
}*/
