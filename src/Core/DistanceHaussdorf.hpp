//
// Created by pierre on 27/12/18.
//

#ifndef PROJECT_DISTANCEHAUSSDORF_HPP
#define PROJECT_DISTANCEHAUSSDORF_HPP

#include "Macros.hpp"

class DistanceHausdorff{
public:
    DistanceHausdorff();

    void setImg(std::string name, int threshold_canny_, Mat img_, VectorPoint2 contour_img_);

    double forward(VectorPoint2 element);
    double forward_tf(VectorPoint2 element, double threshold);
    double forward_ff(VectorPoint2 element, double threshold);
    double forward_tf_prim(VectorPoint2 element, double threshold, int level);
    double forward_ff_prim(VectorPoint2 element, double threshold, int level);

    double reverse_tf(VectorPoint2 img,VectorPoint2 elt, double threshold, Box box);
    double reverse_ff(VectorPoint2 img,VectorPoint2 elt, double threshold, Box box);

    double reverse_max(VectorPoint2 img, VectorPoint2 elt);
    double reverse_max_box(VectorPoint2 img, VectorPoint2 elt, Box box);
    double forward_reverse(double f, double r, int D);

    void computeDeltaPrime(int w, int h,int level);
    void popBackDeltaPrime();
    void cleanDeltaPrime();
private:
    void init();

    std::string id;
    int threshold_canny;
    Mat img;
    VectorPoint2 contour_img;
    std::vector<std::vector<double>> deltaXY;
    std::vector<std::vector<std::vector<double>>> deltaPrims;

};


#endif //PROJECT_DISTANCEHAUSSDORF_HPP
