//
// Created by pierre on 27/12/18.
//

#ifndef PROJECT_MACROS_HPP
#define PROJECT_MACROS_HPP

#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <string>


#include <opencv2/core/mat.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

typedef glm::vec3 Vector3;
typedef glm::vec2 Vector2;
typedef glm::vec4 Vector4;
typedef glm::vec3 Point3;
typedef glm::vec2 Point2;
typedef glm::vec3 Normal;
typedef glm::uvec3 Triangle;
typedef glm::uvec2 Line;
typedef glm::vec3 Color;


typedef std::vector<Point3> VectorPoint3;
typedef std::vector<Point2> VectorPoint2;
typedef std::vector<Normal> VectorNormal;
typedef std::vector<Triangle> VectorTriangles;
typedef std::vector<double> VectorDouble;

typedef glm::mat4 Matrix4;
typedef glm::mat3 Matrix3;
typedef glm::mat2 Matrix2;

typedef cv::Mat Mat;

typedef struct Box{
    int xMin, xMax;
    int yMin, yMax;
}Box;

#define INF std::numeric_limits<double>::infinity()



#endif //PROJECT_MACROS_HPP
