//
// Created by pierre on 27/12/18.
//

#ifndef PROJECT_CANNY_HPP
#define PROJECT_CANNY_HPP

#include "Macros.hpp"


Mat canny(Mat img, int threshold);
VectorPoint2 contours(Mat m);

#endif //PROJECT_CANNY_HPP
