//
// Created by pierre on 27/12/18.
//

#include <Log/Log.hpp>
#include "Tuples.hpp"

Tuples::Tuples(float _a, float _b, float _c, float _d, float _x, float _y) :
    a{_a}, b{_b}, c{_c}, d{_d}, x{_x}, y{_y} {

}

Tuples::Tuples() {

}

Matrix2 Tuples::getRot() {
    return Matrix2(a,b,c,d);
}

Vector2 Tuples::getTrans() {
    return Vector2(x,y);
}

void Tuples::addA(float v) {
    a += v;
}

void Tuples::addB(float v) {
    b +=v;
}

void Tuples::addC(float v) {
    c += v;
}

void Tuples::addD(float v) {
    d += v;
}

void Tuples::addX(float v) {
    x += v;
}

void Tuples::addY(float v) {
    y += v;
}

void Tuples::test() {
    a = 0;
    b = 1;
    c = 2;
    d = 3;
    Matrix2 m = getRot();
    Log(logDebug) << m[0][0] << " " << m[0][1] << " " << m[1][0] << " " << m[1][1];
}

void Tuples::display() {
    Log(logInfo) << a << " " << b << " " << c << " " << d << " " << x << " " << y;
}