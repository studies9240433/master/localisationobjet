//
// Created by pierre on 28/12/18.
//

#include <Log/Log.hpp>
#include <chrono>
#include "Search.hpp"
#include "Canny.hpp"
#include "DiscretiseTransform.hpp"
#include "AffineTransform.hpp"
#include "DistanceHaussdorf.hpp"

typedef std::chrono::high_resolution_clock Clock;

Search::Search() {
    distanceHausdorff = new DistanceHausdorff();
}
bool Search::verifParam() {
    if(!image.data){
        Log(logError) << "No Image loaded";
        return false;
    }
    if(!model.data){
        Log(logError) << "No Model loaded";
        return false;
    }
    return true;
}

void Search::launchSearch(int _distD, int _distd, int _nbValue) {
    Log(logDebug) << "Launch research in image";
    if(!verifParam()) return;


    auto t0 = Clock::now();
    contoursMod = contours(model_contour);
    contoursImg = contours(image_contour);
    distanceHausdorff->setImg(name_img, threshold_canny_image, image_contour, contoursImg);

    auto t1 = Clock::now();
    distD = _distD;
    distd = _distd;
    nbValue = _nbValue;
    switch(methodSearch){
        case 0:
            launchSearchFirstThreshold();
            break;
        case 1:
            launchSearchBestSolutionFf();
            break;
        case 2:
            launchSearchBestSolutionTf();
            break;
        case 3:
            launchSearchFirstThresholdWithReverse();
            break;
        case 4:
            launchSearchBestSolutionFfWithReverse();
            break;
        case 5:
            launchSearchBestSolutionTfWithReverse();
            break;
    }

    auto t2 = Clock::now();
    auto d = std::chrono::duration<double>(t2-t1);
    auto d0 = std::chrono::duration<double>(t1-t0);
    int hour = d.count() / 3600;
    int minutes = d.count() / 60;
    int seconds = d.count();
    int hour0 = d0.count() / 3600;
    int minutes0 = d0.count() / 60;
    int seconds0 = d0.count();
    Log(logInfo) << "Computation time precompute: " << hour0 << "h " << minutes0%60 <<"m " << seconds0%60 << "s";
    Log(logInfo) << "Computation time research: " << hour << "h " << minutes%60 <<"m " << seconds%60 << "s";
    Log(logDebug) << "End research";
    img_from_result = image;
}

void Search::launchSearchFirstThreshold(){
    bool arret = false;
    double score;
    VectorPoint2 contoursModRes;
    bool out;

    DiscretiseTransform dt(image_contour,model_contour,contoursMod,d_min,d_max,skew_max,s_max,trans,methodSearch,distanceHausdorff,
            aMin,aMax,bMin,bMax,cMin,cMax,dMin,dMax);
    dt.setFf(ff);
    dt.setTf(tf);

    dt.begin();

    while(dt.hasNext() && !arret) {

        VectorPoint2 contoursModT = transformContours(contoursMod, image_contour, dt.getItr(), out);

        if(!out){
            score = distanceHausdorff->forward_tf(contoursModT,tf);
            Log(logInfo) << "Hausdorff: " << score;
            dt.displayT();
            if(score > ff ){
                arret = true;
                contoursModRes = contoursModT;
            }
        }
    }
    result = getImgFromContours(contoursModRes,image_contour);
}

void Search::launchSearchBestSolutionFf() {
    bool arret = false;
    double score,score_lim = 0;
    VectorPoint2 contoursModRes;
    bool out;

    DiscretiseTransform dt(image_contour,model_contour,contoursMod,d_min,d_max,skew_max,s_max,trans,methodSearch,distanceHausdorff,
                           aMin,aMax,bMin,bMax,cMin,cMax,dMin,dMax);
    dt.setTf(tf);
    dt.setFf(0);

    dt.begin();

    while(dt.hasNext() && !arret) {

        VectorPoint2 contoursModT = transformContours(contoursMod, image_contour, dt.getItr(), out);

        if(!out){
            score = distanceHausdorff->forward_tf(contoursModT,tf);
            Log(logInfo) << "Hausdorff: " << score;
            dt.displayT();
            if(score > score_lim ){
                score_lim = score;
                dt.setFf(score_lim);
                contoursModRes = contoursModT;
            }

        }
    }
    Log(logDebug) << "Result:";
    Log(logDebug) << " ff = " << score_lim;
    result = getImgFromContours(contoursModRes,image_contour);
}

void Search::launchSearchBestSolutionTf() {
    bool arret = false;
    double score,score_lim = INF;
    VectorPoint2 contoursModRes;
    bool out;

    DiscretiseTransform dt(image_contour,model_contour,contoursMod,d_min,d_max,skew_max,s_max,trans,methodSearch,distanceHausdorff,
                           aMin,aMax,bMin,bMax,cMin,cMax,dMin,dMax);
    dt.setTf(INF);
    dt.setFf(ff);

    dt.begin();

    while(dt.hasNext() && !arret) {

        VectorPoint2 contoursModT = transformContours(contoursMod, image_contour, dt.getItr(), out);

        if(!out){
            score = distanceHausdorff->forward_ff(contoursModT,ff);
            Log(logInfo) << "Hausdorff: " << score;
            dt.displayT();
            if(score < score_lim ){
                score_lim = score;
                dt.setTf(score_lim);
                contoursModRes = contoursModT;
            }

        }
    }
    Log(logDebug) << "Result:";
    Log(logDebug) << " tf = " << score_lim;
    result = getImgFromContours(contoursModRes,image_contour);
}

void Search::launchSearchFirstThresholdWithReverse() {
    bool arret = false;
    double score;
    VectorPoint2 contoursModRes;
    bool out;
    Box box;

    DiscretiseTransform dt(image_contour,model_contour,contoursMod,d_min,d_max,skew_max,s_max,trans,methodSearch,distanceHausdorff,
                           aMin,aMax,bMin,bMax,cMin,cMax,dMin,dMax);

    dt.setTf(tf);
    dt.setFf(ff);

    dt.begin();

    while(dt.hasNext() && !arret) {

        VectorPoint2 contoursModT = transformContoursMinMax(contoursMod, model_contour, image_contour, dt.getItr(), box, out);
        if(!out){
            score = distanceHausdorff->forward_tf(contoursModT,tf);
            Log(logInfo) << "Hausdorff: " << score;
            dt.displayT();
            if(score > ff ){
                score = distanceHausdorff->reverse_tf(contoursImg, contoursModT, tr, box);
                if(score > fr){
                    arret = true;
                    contoursModRes = contoursModT;
                }
            }

        }
    }
    result = getImgFromContours(contoursModRes,image_contour);
}

void Search::launchSearchBestSolutionFfWithReverse() {
    bool arret = false;
    double score_f,score_r,score_lim_f = 0, score_lim_r = 0;
    VectorPoint2 contoursModRes;
    bool out;
    Box box;

    DiscretiseTransform dt(image_contour,model_contour,contoursMod,d_min,d_max,skew_max,s_max,trans,methodSearch,distanceHausdorff,
                           aMin,aMax,bMin,bMax,cMin,cMax,dMin,dMax);

    dt.setTf(tf);
    dt.setFf(0);

    dt.begin();

    while(dt.hasNext() && !arret) {

        VectorPoint2 contoursModT = transformContoursMinMax(contoursMod, model_contour, image_contour, dt.getItr(), box, out);

        if(!out){
            score_f = distanceHausdorff->forward_tf(contoursModT,tf);
            Log(logInfo) << "Hausdorff: " << score_f;
            dt.displayT();
            if(score_f > score_lim_f ){
                score_r = distanceHausdorff->reverse_tf(contoursImg,contoursModT,tr,box);
                if(score_r > score_lim_r){
                    score_lim_f = std::min(score_f,(score_r-pPlu)/pMul);
                    score_lim_r = std::min(score_r,pMul*score_f+pPlu);
                    dt.setFf(score_lim_f);
                    contoursModRes = contoursModT;
                }
            }

        }
    }
    Log(logDebug) << "Result:";
    Log(logDebug) << " ff = " << score_lim_f;
    Log(logDebug) << " fr = " << score_lim_r;
    result = getImgFromContours(contoursModRes,image_contour);

}

void Search::launchSearchBestSolutionTfWithReverse() {
    bool arret = false;
    double score_f, score_r,score_lim_f = INF, score_lim_r = INF;
    VectorPoint2 contoursModRes;
    bool out;
    Box box;

    DiscretiseTransform dt(image_contour,model_contour,contoursMod,d_min,d_max,skew_max,s_max,trans,methodSearch,distanceHausdorff,
                           aMin,aMax,bMin,bMax,cMin,cMax,dMin,dMax);

    dt.setTf(INF);
    dt.setFf(ff);
    dt.begin();

    while(dt.hasNext() && !arret) {

        VectorPoint2 contoursModT = transformContoursMinMax(contoursMod, model_contour, image_contour, dt.getItr(), box, out);

        if(!out){
            score_f = distanceHausdorff->forward_ff(contoursModT,ff);
            Log(logInfo) << "Hausdorff: " << score_f;
            dt.displayT();
            if(score_f < score_lim_f ){
                score_r = distanceHausdorff->reverse_ff(contoursImg, contoursModT, fr, box);
                if(score_r < score_lim_r){
                    score_lim_f = std::min(score_f, (score_r-pPlu)/pMul);
                    score_lim_r = std::min(score_r, score_f*pMul+pPlu);
                    dt.setTf(score_lim_f);
                    contoursModRes = contoursModT;
                }
            }

        }
    }
    Log(logDebug) << "Result:";
    Log(logDebug) << " tf = " << score_lim_f;
    result = getImgFromContours(contoursModRes,image_contour);

}


void Search::setImage(std::string name, Mat i) {
    name_img = name;
    image = i;
    image_contour = canny(image,threshold_canny_image);
}

void Search::setModel(Mat i) {
    model = i;
    model_contour = canny(model,threshold_canny_model);
}

void Search::setThresholdCannyImage(int v) {
    threshold_canny_image=v;
    if(image.data) image_contour = canny(image,threshold_canny_image);
}

void Search::setThresholdCannyModel(int v) {
    threshold_canny_model=v;
    if(model.data) model_contour = canny(model,threshold_canny_model);
}

void Search::setMethod(int method) {
    methodSearch=method;
}

void Search::setAreaMin(double v){
    d_min = v;
}

void Search::setAreaMax(double v){
    d_max = v;
}

void Search::setSkewMax(double v){
    skew_max = v;
}

void Search::setSMax(double v){
    s_max = v;
}

void Search::setTf(double v){
    tf = v;
}

void Search::setTr(double v){
    tr = v;
}


void Search::setFf(double v){
    ff = v;
}

void Search::setFr(double v){
    fr = v;
}

void Search::setpMul(double v){
    pMul = v;
}

void Search::setpPlu(double v){
    pPlu = v;
}

void Search::setTrans(bool b) {
    trans = b;
}

void Search::setFactorImage(float f) {
    factor_image = f;
    afficheResultat();
}

void Search::setFactorResult(float f) {
    factor_result = f;
    afficheResultat();
}

void Search::setAMin(double v){
    aMin = v;
}

void Search::setAMax(double v){
    aMax = v;
}

void Search::setBMin(double v){
    bMin = v;
}

void Search::setBMax(double v){
    bMax = v;
}

void Search::setCMin(double v){
    cMin = v;
}

void Search::setCMax(double v){
    cMax = v;
}

void Search::setDMin(double v){
    dMin = v;
}

void Search::setDMax(double v){
    dMax = v;
}

void Search::setAll(double am, double aM, double bm, double bM, double cm, double cM, double dm, double dM){
    setAMin(am);
    setAMax(aM);
    setBMin(bm);
    setBMax(bM);
    setCMin(cm);
    setCMax(cM);
    setDMin(dm);
    setDMax(dM);
}

void Search::afficheImage() {
    if(!image.data){
        Log(logError) << "No image loaded";
        return;
    }
    cv::imshow("Image",image);
}

void Search::afficheModel() {
    if(!model.data){
        Log(logError) << "No model loaded";
        return;
    }
    cv::imshow("Model",model);
}

void Search::afficheImageContour() {
    if(!image_contour.data){
        Log(logError) << "No image loaded";
        return;
    }
    cv::imshow("Image contour",image_contour);
}

void Search::afficheModelContour() {
    if(!model_contour.data){
        Log(logError) << "No model loaded";
        return;
    }
    cv::imshow("Model contour",model_contour);
}

void Search::afficheResultat() {
    if(!result.data){
        Log(logError) << "No result to display";
        return;
    }
    cv::imshow("Result",result*factor_result+img_from_result*factor_image);
}